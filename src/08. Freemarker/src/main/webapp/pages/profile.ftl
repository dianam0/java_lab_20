<html>
<head><title>Profile</title></head>
<body>
<h1>USER ${id}:</h1>
<p>
    FirstName: <#if firstName??>${firstName}<#else>*NULL</#if><br>
    LastName: <#if lastName??>${lastName}<#else>*NULL</#if><br>
    Age: <#if age ??>${age}</#if><br>
</p>
</body>
</html>