<html>
<head>
    <title>Users</title>
</head>
<body>
<div>
    <#if color ??>
        <h1 style="color: ${color}">USERS</h1>
    <#else><h1>USERS</h1>
    </#if>
    <form action="users" method="post">
        <label>
            <select name="color">
                <option value="red">RED</option>
                <option value="green">GREEN</option>
                <option value="blue">BLUE</option>
            </select>
        </label>
        <input type="submit" value="OK">
    </form>
</div>
<div>
    <table>
        <tr>
            <th>ID</th>
        </tr>
        <#list users as user>
            <tr>
                <td>
                    <form method="get" action="profile" style="margin: 0">
                        <input type="hidden" name="id" value="${user.getId()}">
                        <input type="hidden" name="firstName" value="${user.getFirstName()}">
                        <input type="hidden" name="lastName" value="${user.getLastName()}">
                        <input type="hidden" name="age" value="${user.getAge()}">
                        <input style="border: 0; background: white" type="submit" value="${user.getId()}">
                    </form>
                </td>
            </tr>
        </#list>
    </table>
</div>
</body>
</html>
