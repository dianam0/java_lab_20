package ru.itis.javalab;

import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class Downloader {

	public Downloader() {}

	public void download(String mode, String count, List<String> files, String folder) {
		if (mode.equals("one-thread")) {
			count = "1";
		}
		ExecutorService executorService = Executors.newFixedThreadPool(Integer.parseInt(count));
		String[] filesAr = files.toArray(new String[0]);
		for (int i = 1; i < files.size(); i++) {
			int finalI = i;
			Runnable task  = () -> {
				try (InputStream in = new URL(filesAr[finalI]).openStream()) {
    				Files.copy(in, Paths.get(folder + "\\" + finalI +".jpg"));
    			}
    			catch (IOException e) {
					throw new IllegalStateException(e);
				}
    		};
    		executorService.submit(task);
		}
	}
}