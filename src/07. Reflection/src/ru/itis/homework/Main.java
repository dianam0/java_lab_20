package ru.itis.homework;

import org.postgresql.ds.PGSimpleDataSource;
import ru.itis.homework.model.User;
import ru.itis.homework.util.EntityManager;

public class Main {

    public static void main(String[] args) {
        PGSimpleDataSource source = new PGSimpleDataSource();
        source.setUser("postgres");
        source.setURL("jdbc:postgresql://localhost:5432/java_lab_20");
        source.setPassword("t4f48aoq");

        EntityManager entityManager = new EntityManager(source);
        entityManager.createTable("account", User.class);

        User user = new User(1L, "Chloe", "Dekker", true);
        entityManager.save("account", user);

        User foundUser = entityManager.findById("account", User.class, Long.class, 1L);
        System.out.println(foundUser.getFirstName());
    }
}
