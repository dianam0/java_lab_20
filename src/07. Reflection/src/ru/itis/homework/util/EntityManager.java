package ru.itis.homework.util;

import ru.itis.homework.caster.Caster;

import javax.sql.DataSource;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;

/**
 * 02.11.2020
 * 10. Reflection
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class EntityManager {
    private static DataSource dataSource;

    public EntityManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> void createTable(String tableName, Class<T> entityClass) {
        //language = SQL
        StringBuilder createTable = new StringBuilder("CREATE TABLE IF NOT EXISTS" + tableName + "(");

        Field[] fields = entityClass.getDeclaredFields();
        for (Field field : fields) {
            createTable.append(field.getName() + " " + Caster.cast(field) + ",");
        }

        createTable.deleteCharAt(createTable.length() - 1);
        createTable.append(");");

        Connection connection = getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeQuery(createTable.toString());
        } catch (SQLException ignored) {
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }
            try {
                connection.close();
            } catch (SQLException ignored) {
            }
        }
    }

    public void save(String tableName, Object entity) {
        Class<?> classOfEntity = entity.getClass();

        //language = SQL
        StringBuilder insertIntoTable = new StringBuilder("INSERT INTO " + tableName + "(");
        Field[] fields = classOfEntity.getDeclaredFields();
        for (Field field : fields) {
            insertIntoTable.append(field.getName() + ",");
        }

        insertIntoTable.deleteCharAt(insertIntoTable.length() - 1);
        insertIntoTable.append(") VALUES(");

        for (Field field : fields) {
            try {
                Field objectField = classOfEntity.getDeclaredField(field.getName());
                objectField.setAccessible(true);
                String fieldValue = (String) objectField.get(entity).toString();

                insertIntoTable.append("'" + fieldValue + "',");
            } catch (NoSuchFieldException | IllegalAccessException e) {
            }
        }
        insertIntoTable.deleteCharAt(insertIntoTable.length() - 1);
        insertIntoTable.append(");");

        Connection connection = getConnection();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(insertIntoTable.toString());
        } catch (SQLException ignored) {
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }
            try {
                connection.close();
            } catch (SQLException ignored) {
            }
        }
    }

    public <T, ID extends Number> T findById(String tableName, Class<T> resultType, Class<ID> idType, ID idValue) {
        //language = SQL
        StringBuilder selectObject = new StringBuilder("SELECT * FROM " + tableName + " WHERE id = " + idValue);

        Connection connection = getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        T result = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectObject.toString());
            if (resultSet.next()) {
                Field[] fields = resultType.getDeclaredFields();
                String[] values = new String[fields.length];
                int i = 0;

                for (Field field : fields) {
                    String value = resultSet.getString(field.getName());
                    values[i] = value;
                    i++;
                }
                i = 0;
                try {
                    result = resultType.newInstance();
                    for (Field field : fields) {
                        Field objectField = null;
                        try {
                            objectField = resultType.getDeclaredField(field.getName());
                        } catch (NoSuchFieldException e) {
                            throw new IllegalArgumentException(e);
                        }
                        objectField.setAccessible(true);
                        String objectFieldType = objectField.getType().getSimpleName().toLowerCase();
                        if (objectFieldType.equals("long")) {
                            objectField.set(result, Long.parseLong(values[i]));
                        }
                        else if (objectFieldType.equals("boolean")) {
                            if (values[i].equals("t")) {
                                objectField.set(result, true);
                            }
                            else {
                                objectField.set(result, false);
                            }
                        }
                        else {
                            objectField.set(result, values[i]);
                        }
                        i++;
                    }

                } catch (InstantiationException | IllegalAccessException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        } catch (SQLException ignored) {
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }
            try {
                connection.close();
            } catch (SQLException ignored) {
            }
        }

        return result;
    }

    private static Connection getConnection() {
        Connection connection;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        return connection;
    }
}
