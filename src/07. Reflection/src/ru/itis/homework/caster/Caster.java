package ru.itis.homework.caster;

import java.lang.reflect.Field;

public class Caster {
    public static String cast(Field field) {
        String nameType = field.getType().getSimpleName().toLowerCase();

        if (nameType.equals("long")) {
            return "INT";
        }
        else if (nameType.equals("boolean")) {
            return "BOOLEAN";
        }
        else {
            return "VARCHAR(255)";
        }
     }
}
