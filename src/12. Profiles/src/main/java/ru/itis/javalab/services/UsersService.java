package ru.itis.javalab.services;

import ru.itis.javalab.dto.UserDto;

public interface UsersService {

    void addUser(UserDto userDto);

}
