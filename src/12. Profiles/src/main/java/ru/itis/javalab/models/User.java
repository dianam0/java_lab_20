package ru.itis.javalab.models;

import lombok.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class User {
    private Long id;
    private String name;
    private String email;
    private String password;

    private State state;
    public enum State {
        CONFIRMED, NOT_CONFIRMED
    }

    private String confirmCode;
}
