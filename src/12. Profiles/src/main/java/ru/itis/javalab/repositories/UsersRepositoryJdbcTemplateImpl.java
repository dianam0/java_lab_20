package ru.itis.javalab.repositories;

import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.itis.javalab.models.User;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into account(name, email, password, state, confirm_code) values (:name, :email, :password, :state, :confirm_code)";

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    @Override
    public void save(User entity) {
        Map<String, Object> params = new HashMap<>();

        params.put("name", entity.getName());
        params.put("email", entity.getEmail());
        params.put("password", entity.getPassword());
        params.put("state", entity.getState().toString());
        params.put("confirm_code", UUID.fromString(entity.getConfirmCode()));

        namedParameterJdbcTemplate.update(SQL_INSERT_USER, params);
    }
}
