package ru.itis.javalab.repositories;

public interface CrudRepository<T> {

    void save(T entity);
}
