package ru.itis.javalab.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.javalab.dto.UserDto;
import ru.itis.javalab.services.UsersService;

@Controller
public class SignUpController {

    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String geSignUpPage() {
        return "sign_up";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signUp(UserDto userDto) {

        usersService.addUser(userDto);
        return "redirect:/signUp";
    }

}
