import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class TcpClient {

    public String ip;
    public int port;
    public Socket socket;
    public static String clientName;

    public TcpClient(String ip, int port) {
        this.ip = ip;
        this.port = port;
        try {
            this.socket = new Socket(ip, port);
            this.enterName();
            new Writer(socket.getOutputStream()).start();
            new Reader(socket.getInputStream()).start();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void enterName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press your nick: ");
        this.clientName = scanner.nextLine();
    }
}
