import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Args {
    @Parameter(names = {"--ip"}, variableArity = true)
    public String ip;

    @Parameter(names = {"--port"})
    public Integer port;
}
