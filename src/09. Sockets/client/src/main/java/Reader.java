import java.io.*;

public class Reader extends Thread {

    private InputStream in;
    public Reader(InputStream in) {
        this.in = in;
    }

    @Override
    public void run() {
        try {
            while (true) {
                DataInputStream in = new DataInputStream(this.in);
                System.out.println(in.readUTF());
            }
        }
        catch (IOException e) { }
    }

    public void close() {
        try {
            in.close();
        }
        catch (IOException ignored) { }
    }
}
