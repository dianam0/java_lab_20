import java.io.*;
import java.util.Scanner;

public class Writer extends Thread {

    private OutputStream out;

    public Writer(OutputStream out) {
        this.out = out;
    }

    @Override
    public void run() {
        while (true) {
            try {
                DataOutputStream out = new DataOutputStream(this.out);
                Scanner scan = new Scanner(System.in);
                out.writeUTF(TcpClient.clientName + ": " + scan.nextLine());
                out.flush();
            } catch (IOException ignored) { }
        }
    }

    public void close() {
        try {
            out.close();
        } catch (IOException ignored) {
        }
    }
}
