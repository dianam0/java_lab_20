import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

class TcpServer extends Thread {

    private ServerSocket serverSocket;
    List<ClientHandler> clients;

    public TcpServer(int port) {
        try {
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        clients = new ArrayList<ClientHandler>();
    }
    @Override
    public void run() {
        while (true) {
            try {
                Socket socket = serverSocket.accept();
                subscribe(socket);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    public void subscribe(Socket socket) {
        ClientHandler client = new ClientHandler(socket, this);
        clients.add(client);
        client.start();
    }

    public void broadcast(final String message) {
        clients.forEach(client -> client.sendMessage(message));
    }
}