import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ClientHandler extends Thread {

    private Socket socket;
    private TcpServer server;
    public ClientHandler(Socket socket, TcpServer server) {
        this.socket = socket;
        this.server = server;
    }

    @Override
    public void run() {
        while (true) {
            try {
                DataInputStream in = new DataInputStream(socket.getInputStream());
                this.server.broadcast(in.readUTF());
            } catch (IOException | NullPointerException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }

    public void sendMessage(String message) {
        try {
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeUTF(message);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
