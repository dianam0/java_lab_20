<html lang="ru">
<head>
    <title>PROFILE</title>
</head>
<body>
<#if name ??>
    <h2>Hello, ${name}!</h2>
</#if>

<form method="post" action="profile">
    <input type="hidden" name="csrf_token" value="${csrf_token}">
    <input type="submit" name="action" value="delete">
</form>
</body>
</html>