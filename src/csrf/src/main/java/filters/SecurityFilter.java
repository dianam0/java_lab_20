package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        if (request.getParameter("action") != null && request.getParameter("action").equals("delete")) {
            HttpSession session = request.getSession();
            String csrf = request.getParameter("csrf_token");
            System.out.println(csrf);
            System.out.println(session.getAttribute("csrf_token"));
            if (!session.getAttribute("csrf_token").equals(csrf)) {
                response.sendError(403);
                return;
            }
        }
        chain.doFilter(request, response);



    }

    @Override
    public void destroy() {

    }
}
