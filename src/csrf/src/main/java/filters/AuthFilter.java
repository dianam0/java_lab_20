package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/profile")
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        HttpSession session = request.getSession();
        if (session != null && session.getAttribute("user") != null) {
            req.getRequestDispatcher("/profile").forward(req,resp);
            System.out.println("profile");
        }
        else {
            req.getRequestDispatcher("/login").forward(req,resp);
            System.out.println("login");
        }
    }

    @Override
    public void destroy() {}
}
