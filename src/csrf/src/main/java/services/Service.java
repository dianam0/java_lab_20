package services;

import models.User;

import java.util.Optional;

public interface Service {

    public Optional<User> getUserByEmailAndPassword(String email, String password);

    public void deleteUser(User user);
}
