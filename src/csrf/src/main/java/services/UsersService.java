package services;

import models.User;
import repositories.UsersRepository;

import java.util.Optional;

public class UsersService implements Service {

    private UsersRepository usersRepository;

    public UsersService(UsersRepository repository) {
        this.usersRepository = repository;
    }

    @Override
    public Optional<User> getUserByEmailAndPassword(String email, String password) {
        return usersRepository.findByEmailAndPassword(email, password);
    }

    @Override
    public void deleteUser(User user) {
        usersRepository.update(user);
    }
}
