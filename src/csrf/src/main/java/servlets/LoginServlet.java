package servlets;

import models.User;
import services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private UsersService service;

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        service = (UsersService) servletContext.getAttribute("usersService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/view/login.ftl").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<User> user = service.getUserByEmailAndPassword(req.getParameter("email"), req.getParameter("password"));
        if (user.isPresent()) {
            req.getSession().setAttribute("user", user.get());

            String csrf = UUID.randomUUID().toString();
            req.getSession().setAttribute("csrf_token", csrf);

            resp.sendRedirect("/csrf_war/profile");
        }
        else {
            req.getRequestDispatcher("/view/login.ftl").forward(req, resp);
        }

    }
}
