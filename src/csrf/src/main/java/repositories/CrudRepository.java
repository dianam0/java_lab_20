package repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {

    List<T> findAll();
    void save(T entity);
    Optional<T> findById(Long id);
    void update(T entity);
}
