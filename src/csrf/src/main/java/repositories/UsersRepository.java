package repositories;

import models.User;

import java.util.Optional;

public interface UsersRepository extends CrudRepository<User> {

    @Override
    Optional<User> findById(Long id);

    @Override
    void update(User entity);

    Optional<User> findByEmailAndPassword(String email, String password);
}
