package repositories;

import com.zaxxer.hikari.HikariDataSource;
import models.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "UPDATE \"user\" SET \"name\" = :name, password = :password, is_deleted = :is_deleted WHERE id = :id";

    //language=SQL
    private static final String SQL_SELECT_BY_EMAIL_AND_PASSWORD = "SELECT * FROM \"user\" WHERE email = :email AND password = :password AND is_deleted != true";

    private RowMapper<User> userRowMapper = (row, i) -> User.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public void save(User entity) {

    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public void update(User entity) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", entity.getName());
        params.put("password", entity.getPassword());
        params.put("is_deleted", true);
        params.put("id", entity.getId());

        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, params);
    }

    @Override
    public Optional<User> findByEmailAndPassword(String email, String password) {
        Map<String, Object>  params = new HashMap<>();
        params.put("email", email);
        params.put("password", password);

        User user = null;
        try {
            user = namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL_AND_PASSWORD, params, userRowMapper);
        }
        catch (EmptyResultDataAccessException e) {}

        return Optional.ofNullable(user);
    }
}
