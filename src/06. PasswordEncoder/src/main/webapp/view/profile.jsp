<%@ page import="models.User" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    PrintWriter printWriter = response.getWriter();

    HttpSession httpSession = request.getSession();
    User user = (User) httpSession.getAttribute("user");
    String name = user.getName();
    printWriter.println("<h3>Это профиль пользователя " + name + "</h3>");
%>
</body>
</html>
