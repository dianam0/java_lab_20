package repositories;

import models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CRUDRepository<User> {

    @Override
    List<User> findAll();

    @Override
    Optional<User> findById(Long id);

    @Override
    void save(User entity);

    @Override
    void update(User entity);

    Optional<User> findByEmail(String email);
}
