package repositories;

import models.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserRepositoryImpl implements UserRepository {

    //language = SQL
    private static String SQL_INSERT = "INSERT INTO \"user\"(name, email, password) values(:name, :email, :password)";

    //language = SQL
    private static String SQL_SELECT_WHERE_EMAIL = "SELECT * FROM \"user\" WHERE email = ?";

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private RowMapper<User> userRowMapper = (row, i) -> User.builder()
            .id(row.getLong("id"))
            .name(row.getString("name"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();

    public UserRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<User> findAll() {
        return null;
    }

    public Optional<User> findById(Long id) {
        return Optional.empty();
    }

    public void update(User entity) {}

    public void save(User entity) {
        Map<String, Object> params = new HashMap<>();
        params.put("name", entity.getName());
        params.put("email", entity.getEmail());
        params.put("password", entity.getPassword());

        namedParameterJdbcTemplate.update(SQL_INSERT, params);
    }

    public Optional<User> findByEmail(String email) {
        User user;
        try {
            user = jdbcTemplate.queryForObject(SQL_SELECT_WHERE_EMAIL, userRowMapper, email);
        } catch (EmptyResultDataAccessException e) {
            user = null;
        }
        return Optional.ofNullable(user);
    }
}
