package services;

import models.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import repositories.UserRepository;

import java.util.Optional;

public class UserServiceImpl implements UserService {
    private UserRepository usersRepository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository usersRepository) {
        this.usersRepository = usersRepository;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    public void addUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        usersRepository.save(user);
    }

    public Optional<User> getUserByEmailAndPassword(String email, String password) {
        Optional<User> user = usersRepository.findByEmail(email);

        if (user.isPresent()) {
            if (!passwordEncoder.matches(password, user.get().getPassword())) {
                return Optional.empty();
            }
        }

        return user;
    }
}
