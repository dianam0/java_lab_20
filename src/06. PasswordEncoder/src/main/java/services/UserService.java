package services;

import models.User;

import java.util.Optional;

public interface UserService {

    public void addUser(User user);

    public Optional<User> getUserByEmailAndPassword(String email, String password);
}
