//package filters;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.logging.FileHandler;
//import java.util.logging.SimpleFormatter;
//
//@WebFilter(servletNames = "HelloServlet")
//public class LoggerFilter implements Filter {
//    private Logger logger;
//
//    @Override
//    public void init(FilterConfig filterConfig) throws ServletException {
//        logger = LoggerFactory.getLogger(LoggerFilter.class);
//        FileHandler fileHandler = null;
//        try {
//            fileHandler = new FileHandler("logger", 1000, 7);
//            fileHandler.setFormatter(new SimpleFormatter());
//            logger.info("as");
//        } catch (IOException e) {
//            //throw new IllegalArgumentException(e);
//        }
//    }
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        logger.info(request.getRequestURI());
//        System.out.println(logger.getName());
//        logger.info("Just a log message.");
//        logger.debug("Message for debug level.");
//        try {
//            Files.readAllBytes(Paths.get("C:\\Users\\qiiau\\kpfu\\java_lab_20\\src\\logging\\src\\main\\logs\\logger.log"));
//        }
//        catch (IOException ioex) {
//            logger.error("Failed to read file {}.", "C:\\Users\\qiiau\\kpfu\\java_lab_20\\src\\logging\\src\\main\\logs\\logger.log", ioex);
//        }
//        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
//        logger.info(httpServletRequest.getRequestURI());
//        filterChain.doFilter(servletRequest, servletResponse);
//
//    }
//
//    @Override
//    public void destroy() {
//
//    }
//}
