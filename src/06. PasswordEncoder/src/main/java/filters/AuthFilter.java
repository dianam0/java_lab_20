package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(servletNames = {"ProfileServlet"})
public class AuthFilter implements Filter {
    private static final String URL = "jdbc:postgresql://localhost:5432/cookies";
    private static final String USER = "postgres";
    private static final String PASSWORD = "t4f48aoq";

    public void init(FilterConfig config) throws ServletException { }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpSession session = request.getSession();
        System.out.println("AUTH");
        if (session.getAttribute("user") == null) {
            req.getRequestDispatcher("/login").forward(req, resp);
        }
        else {
            req.getRequestDispatcher("/profile").forward(req,resp);
        }
    }

    public void destroy() { }
}
