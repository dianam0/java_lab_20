package pool;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;

public class ThreadPool {
    private static Deque<Runnable> tasks;
    private static ThreadPool.PoolWorker[] pool;

    public static ThreadPool newPool(int threadsCount) {
        ThreadPool threadPool = new ThreadPool();
        tasks = new ConcurrentLinkedDeque<>();
        pool = new ThreadPool.PoolWorker[threadsCount];

        for (int i = 0; i < pool.length; i++) {
            pool[i] = new ThreadPool.PoolWorker();
            pool[i].start();
        }

        return threadPool;
    }

    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.notify();
            tasks.addLast(task);
        }
    }

    private static class PoolWorker extends Thread {

        @Override
        public void run() {
            while (true) {
                synchronized (tasks) {
                    try {
                        if (tasks.isEmpty()) {
                            tasks.wait();
                        }
                    }
                    catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                tasks.pop().run();
            }
        }
    }
}
