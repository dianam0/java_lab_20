package ru.itis.javalab.repositories;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SimpleJdbcTemplate {

    private Connection connection;
    private DataSource dataSource;

    public SimpleJdbcTemplate(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public <T> List<T> query(String sql, RowMapper<T> rowMapper, Object ... args) {
        try {
            ResultSet resultSet = null;

            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            int i = 1;
            for (Object arg : args) {
                preparedStatement.setObject(i, arg);
                i++;
            }
            resultSet = preparedStatement.executeQuery();

            List<T> result = new ArrayList<>();
            if (resultSet == null) {
                throw new SQLException("Result is empty");
            }

            while (resultSet.next()) {
                result.add(rowMapper.mapRow(resultSet));
            }

            return result;
        }
        catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }
}
