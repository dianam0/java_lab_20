package repositories;

import javax.servlet.http.Cookie;
import java.sql.*;

public class CookieRepository implements CRUDRepository<Cookie> {
    Connection connection;

    public CookieRepository(Connection connection) {
        this.connection = connection;
    }


    public boolean findByValue(String value) {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery("select * from cookie where cookie_value = '" + value + "'");
            if (result.next()) {
                return true;
            }
            else  {
                return false;
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new IllegalStateException(e);
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    public void save(String value) {
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            final String SQL = "insert into cookie (cookie_value) values ('" + value +  "')";

            statement = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            int affectedRows = statement.executeUpdate();

            if (affectedRows > 0) { }
        }
        catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    throw new IllegalStateException(e);
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
