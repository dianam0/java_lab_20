package repositories;

public interface CRUDRepository<T> {
    boolean findByValue(String value);

    void save(String value);
}
