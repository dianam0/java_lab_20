package controllers;

import com.fasterxml.uuid.Generators;
import repositories.CookieRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final String URL = "jdbc:postgresql://localhost:5432/cookies";
    private static final String USER = "postgres";
    private static final String PASSWORD = "t4f48aoq";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("view/login.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            CookieRepository cookieRepository = new CookieRepository(connection);

            UUID uuid = Generators.timeBasedGenerator().generate();
            cookieRepository.save(uuid.toString());

            Cookie cookie = new Cookie("Auth", uuid.toString());
            cookie.setMaxAge(60);
            resp.addCookie(cookie);
        }
        catch (SQLException e) {
            throw  new  IllegalArgumentException(e);
        }

        req.getRequestDispatcher("view/login.jsp").forward(req, resp);
    }
}
