package filters;

import repositories.CookieRepository;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebFilter(servletNames = {"ProfileServlet"})
public class AuthFilter implements Filter {
    private static final String URL = "jdbc:postgresql://localhost:5432/cookies";
    private static final String USER = "postgres";
    private static final String PASSWORD = "t4f48aoq";

    public void init(FilterConfig config) throws ServletException { }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

        boolean flag = false;
        Cookie[] cookies = request.getCookies();

        if (cookies != null) {
            for (Cookie cookie: cookies) {
                if (cookie.getName().equals("Auth")) {
                    Connection connection = null;
                    try {
                        connection = DriverManager.getConnection(URL, USER, PASSWORD);
                        CookieRepository cookieRepository = new CookieRepository(connection);
                        if (cookieRepository.findByValue(cookie.getValue())) {
                            flag = true;
                        }
                    }
                    catch (SQLException e) {
                        throw  new  IllegalArgumentException(e);
                    }
                }
            }
        }
        if (flag) {
            req.getRequestDispatcher("/profile").forward(req, resp);
        }
        else {
            req.getRequestDispatcher("/login").forward(req, resp);
        }
    }

    public void destroy() { }
}
