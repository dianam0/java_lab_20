package filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

@WebFilter(servletNames = "HelloServlet")
public class LoggerFilter implements Filter {
    private Logger logger;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger = LoggerFactory.getLogger(LoggerFilter.class);
        FileHandler fileHandler = null;
        try {
            fileHandler = new FileHandler("C:\\Users\\qiiau\\kpfu\\java_lab_20\\src\\05. Logging\\src\\main\\resources\\logger.log", 1000, 7);
            fileHandler.setFormatter(new SimpleFormatter());

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        logger.info(request.getRequestURI());
        logger.info("Just a log message.");
        logger.debug("Message for debug level.");
        try {
            Files.readAllBytes(Paths.get("C:\\Users\\qiiau\\kpfu\\java_lab_20\\src\\05. Logging\\src\\main\\resources\\logger.log"));
        }
        catch (IOException ioex) {
            logger.error("Failed to read file {}.", "C:\\Users\\qiiau\\kpfu\\java_lab_20\\src\\05. Logging\\src\\main\\resources\\logger.log", ioex);
        }
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        logger.info(httpServletRequest.getRequestURI());
        filterChain.doFilter(servletRequest, servletResponse);

    }

    @Override
    public void destroy() {
    }
}
