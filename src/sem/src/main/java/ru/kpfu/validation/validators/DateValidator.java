package ru.kpfu.validation.validators;

import ru.kpfu.validation.annotations.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DateValidator implements ConstraintValidator<Date, String>  {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return value.matches(".*(\\d{4}([-/])\\d{2}([-/])\\d{2}).*");
    }
}
