package ru.kpfu.validation.validators;

import ru.kpfu.validation.annotations.UserName;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserNameValidator implements ConstraintValidator<UserName, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        return value.length() < 21 && value.length() > 2 &&
                ((value.matches(".*[A-Z].*") || value.matches(".*[a-z].*")) ||
                (value.matches(".*[А-Я].*") || value.matches(".*[а-я].*"))) &&
                !(value.matches(".*[\\d].*")) && !(value.matches(".*[^A-Za-z0-9].*")) ;
    }
}
