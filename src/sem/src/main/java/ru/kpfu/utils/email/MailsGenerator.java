package ru.kpfu.utils.email;

public interface MailsGenerator {

    String getMailForConfirm(String serverUrl, String code);
}
