package ru.kpfu.dto;

import lombok.Data;
import ru.kpfu.validation.annotations.Date;
import ru.kpfu.validation.annotations.Password;
import ru.kpfu.validation.annotations.PhoneNumber;
import ru.kpfu.validation.annotations.UserName;

import javax.validation.constraints.Email;

@Data
public class UserForm {
    @Email(message = "{errors.incorrect.email}")
    private String email;

    @UserName(message = "{errors.incorrect.user_name}")
    private String name;

    @Password(message = "{errors.incorrect.password}")
    private String password;

    @PhoneNumber(message = "{errors.incorrect.phone_number}")
    private String number;

    @Date(message = "{errors.incorrect.birthday}")
    private String birthday;

    private String animals;
    private String talks;
    private String music;
    private String smoke;
    private String luggage;

}
