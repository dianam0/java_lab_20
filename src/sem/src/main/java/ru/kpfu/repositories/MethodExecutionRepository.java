package ru.kpfu.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.models.MethodExecution;

public interface MethodExecutionRepository extends JpaRepository<MethodExecution, Long> {
}
