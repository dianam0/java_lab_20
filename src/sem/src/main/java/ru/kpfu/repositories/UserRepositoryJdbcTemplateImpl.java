package ru.kpfu.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import ru.kpfu.models.User;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.*;

//public class UserRepositoryJdbcTemplateImpl extends UserRepository {
//
//    private JdbcTemplate jdbcTemplate;
//    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
//
//    //language = SQL
//    private static final String SQL_INSERT_USER = "INSERT INTO \"user\" (password, email, created_at, \"state\", confirm_code) VALUES(:password, :email, :created_at, :state, :confirm_code)";
//
//    //language = SQL
//    private static final String SQL_UPDATE_USER = "UPDATE \"user\" SET password = ?, email = ?, state = ?";
//
//    //language = SQL;
//    private static final String SQL_FIND_USER = "SELECT id FROM \"user\" WHERE password = ? AND email = ?";
//
//    public UserRepositoryJdbcTemplateImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
//    }
//
//    private RowMapper<User> userRowMapper = (row, i) -> User.builder()
//            .id(row.getLong("id"))
//            .creationDate(row.getDate("created_at").toString())
//            .email(row.getString("email"))
//            .password(row.getString("password"))
//            .state(User.State.valueOf(row.getString("state")))
//            .confirmCode(row.getString("confirm_code"))
//            .build();
//
//    public Long save(User entity) {
//        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
//        namedParameters.addValue("created_at", Date.valueOf(entity.getCreationDate()));
//        namedParameters.addValue("email", entity.getEmail());
//        namedParameters.addValue("password", entity.getPassword());
//        namedParameters.addValue("confirm_code", UUID.fromString(entity.getConfirmCode()));
//        namedParameters.addValue("state", entity.getState().toString());
//
//        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
//        namedParameterJdbcTemplate.update(SQL_INSERT_USER,  namedParameters, keyHolder);
//        Long id = (Long) Objects.requireNonNull(keyHolder.getKeys()).get("id");
//        entity.setId(id);
//        return id;
//    }
//
//    public boolean update(User entity) {
//        return jdbcTemplate.update(SQL_UPDATE_USER, entity.getPassword(), entity.getEmail(), entity.getCreationDate()) != 0;
//    }
//
//    public Optional<User> findByEmailAndPassword(String email, String password) {
//        Optional<User> user;
//
//        try {
//            user = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_FIND_USER, userRowMapper, password, email));
//        }
//        catch (EmptyResultDataAccessException e) {
//            user = Optional.empty();
//        }
//
//        return user;
//    }
//}
