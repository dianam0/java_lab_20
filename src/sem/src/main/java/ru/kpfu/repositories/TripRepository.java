package ru.kpfu.repositories;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.models.Trip;

import java.util.List;
import java.util.Optional;

@Repository
public class TripRepository implements  CrudRepository<Trip> {
    @Override
    public List<Trip> findAll() {
        return null;
    }

    @Override
    public Optional<Trip> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Long save(Trip entity) {
        return null;
    }

    @Override
    public boolean update(Trip entity) {
        return false;
    }

    public List<Trip> findByRouteDateAndPrice(String from, String to, String date, Integer priceFrom, Integer priceTo) {
        return null;
    }
}
