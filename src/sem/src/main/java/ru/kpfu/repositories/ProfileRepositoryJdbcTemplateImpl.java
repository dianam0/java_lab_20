//package ru.kpfu.repositories;
//
//
//import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import ru.kpfu.models.Profile;
//import ru.kpfu.models.User;
//import ru.kpfu.models.UserPerformances;
//
//import javax.sql.DataSource;
//import java.sql.Date;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Optional;
//
//public class ProfileRepositoryJdbcTemplateImpl extends ProfileRepository {
//
//    //language = SQL
//    private final static String SQL_SELECT_BY_ID = "SELECT * FROM profile WHERE id = ?";
//
//    //language = SQL
//    private final static String SQL_INSERT = "INSERT INTO profile (\"name\", birthday, \"number\", id, performances_id) VALUES(:name, :birthday, :number, :id, :performances_id)";
//
//    //language = SQL
//    private final static String SQL_UPDATE = "UPDATE profile SET name = :name, birthday = :birthday, number = :number, performances_id = :performances_id)";
//
//    private JdbcTemplate jdbcTemplate;
//    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
//
//    public ProfileRepositoryJdbcTemplateImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
//    }
//
//    private RowMapper<Profile> profileRowMapper = (row, i) -> Profile.builder()
//            .id(row.getLong("id"))
//            .name(row.getString("name"))
//            .number(row.getString("number"))
//            .birthday(row.getDate("birthday").toString())
//            .user(User.builder().id(row.getLong("id")).build())
//            .performances(UserPerformances.builder().id(row.getInt("performances_id")).build())
//            .build();
//
//    public Optional<Profile> findById(Long id) {
//        Optional<Profile> profile;
//
//        try {
//            profile = Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, profileRowMapper, id));
//        } catch (EmptyResultDataAccessException e) {
//            profile = Optional.empty();
//        }
//
//        return profile;
//    }
//
//    public Long save(Profile entity) {
//        Map<String, Object> params = new HashMap<>();
//        params.put("name", entity.getName());
//        params.put("birthday", Date.valueOf(entity.getBirthday()));
//        params.put("number", entity.getNumber());
//        params.put("id", entity.getUser().getId());
//        params.put("performances_id", entity.getPerformances().getId());
//
//        namedParameterJdbcTemplate.update(SQL_INSERT, params);
//
//        return entity.getUser().getId();
//    }
//
//    public boolean update(Profile entity) {
//        Map<String, Object> params = new HashMap<>();
//        params.put("name", entity.getName());
//        params.put("birthday", entity.getBirthday());
//        params.put("number", entity.getNumber());
//        params.put("performances_id", entity.getPerformances().getId());
//
//        return jdbcTemplate.update(SQL_UPDATE, params, profileRowMapper) != 0;
//    }
//}

