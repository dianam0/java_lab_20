package ru.kpfu.repositories;

import org.springframework.stereotype.Repository;
import ru.kpfu.models.Review;

import java.util.List;
import java.util.Optional;

@Repository
public class ReviewRepository implements CrudRepository<Review> {
    @Override
    public List<Review> findAll() {
        return null;
    }

    @Override
    public Optional<Review> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Long save(Review entity) {
        return  null;
    }

    @Override
    public boolean update(Review entity) {
        return false;
    }

    public List<Review> findAllByUserId(Long id) {
        return null;
    }
}
