package ru.kpfu.repositories;

import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.kpfu.models.Review;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReviewRepositoryJdbcTemplateImpl extends ReviewRepository {

    //language = SQL
    private final static String SQL_INSERT = "INSERT INTO review (author_name, profile_id, mark, text) VALUES (:author_name, :profile_id, :mark, :text)";

    //language = SQL
    private final static String SQL_SELECT_BY_PROFILE_ID = "SELECT * FROM \"review\" WHERE profile_id = ?";

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private RowMapper<Review> reviewRowMapper = (row, i) -> Review.builder()
            .id(row.getLong("id"))
            .authorName(row.getString("author_name"))
            .profileId(row.getLong("profile_id"))
            .mark(row.getByte("mark"))
            .text(row.getString("text"))
            .build();

    public ReviewRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public Long save(Review entity) {
        return null;
    }

    @Override
    public List<Review> findAllByUserId(Long id) {
        return jdbcTemplate.query(SQL_SELECT_BY_PROFILE_ID, reviewRowMapper, id);
    }
}
