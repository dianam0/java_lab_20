package ru.kpfu.repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.kpfu.models.Profile;
import ru.kpfu.models.Trip;
import ru.kpfu.models.User;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripRepositoryJdbcTemplateImpl extends TripRepository {

    //language = SQL
    private final static String SQL_SELECT_BY_ROUTE_AND_DATE = "SELECT * FROM \"trip\" WHERE point_From = :point_from AND point_To = :point_to AND date_trip = :date_trip AND date_trip >= current_date AND price >= :priceFrom AND price <= :priceTo ORDER BY id DESC";

    //language = SQL
    private final static String SQL_INSERT_TRIP = "INSERT INTO \"trip\" (driver, point_from, point_to, date_trip, time_trip, price, transport) VALUES(:driver, :point_from, :point_to, :date_trip, :time_trip, :price, :transport)";

    //language = SQL
    private final static String SQL_INSERT_NEW_COMPANION = "UPDATE \"trip\" SET :companion = :companion_id WHERE id = :id AND :companion IS NULL";

    //language = SQL
    private final static String SQL_SELECT_USER_TRIPS = "SELECT * FROM \"trip\" WHERE driver = :driver OR companion1 = :companion1 OR companion2 = :companion2";

    //language = SQL
    private final static String SQL_DELETE_COMPANION = "UPDATE \"trip\" SET :companion = null WHERE id = :id";

    private RowMapper<Trip> tripRowMapper = (row, i) -> Trip.builder()
            .id(row.getLong("id"))
            .driver(new Profile(row.getLong("driver")))
            .companion1(new Profile(row.getLong("companion1")))
            .companion2(new Profile(row.getLong("companion2")))
            .pointFrom(row.getString("point_from"))
            .pointTo(row.getString("point_to"))
            .date(row.getDate("date_trip").toString())
            .price(row.getInt("price"))
            .time(row.getTime("time_trip").toString())
            .transport(row.getString("transport"))
            .build();

    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    public TripRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Trip> findByRouteDateAndPrice(String from, String to, String date, Integer priceFrom, Integer priceTo) {
        Map<String, Object> params = new HashMap<>();

        params.put("point_from", from);
        params.put("point_to", to);
        params.put("date_trip", Date.valueOf(date));
        params.put("priceFrom", priceFrom);
        params.put("priceTo", priceTo);

        return namedParameterJdbcTemplate.query(SQL_SELECT_BY_ROUTE_AND_DATE, params, tripRowMapper);
    }

    public Long save(Trip entity) {
        return null;

    }

    public boolean update(Trip entity, Long id, String companion) {
        Map<String, Object> params = new HashMap<>();

        params.put("companion", companion);
        params.put("companion_id", id);
        params.put("id", entity.getId());

        return namedParameterJdbcTemplate.update(SQL_INSERT_NEW_COMPANION, params) != 0;
    }

    public List<Trip> findByUserId(Long id) {
        Map<String, Object> params = new HashMap<>();

        params.put("driver", id);
        params.put("companion1", id);
        params.put("companion2", id);

        return namedParameterJdbcTemplate.query(SQL_SELECT_USER_TRIPS, params, tripRowMapper);
    }

    public void delete(Trip entity, String companion) {
        Map<String, Object> params = new HashMap<>();

        params.put("companion", companion);
        params.put("id", entity.getId());

        namedParameterJdbcTemplate.update(SQL_DELETE_COMPANION, params);
    }
}
