package ru.kpfu.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.models.Profile;

import java.util.Optional;


public interface ProfileRepository extends JpaRepository<Profile, Long> {

}
