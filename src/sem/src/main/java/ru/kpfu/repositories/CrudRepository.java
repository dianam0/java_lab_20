package ru.kpfu.repositories;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {
    List<T> findAll();
    Optional<T> findById(Long id);
    Long save(T entity);
    boolean update(T entity);
}
