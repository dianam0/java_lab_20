package ru.kpfu.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.models.UserPerformances;

public interface UserPerformancesRepository extends JpaRepository<UserPerformances, Integer> {

}
