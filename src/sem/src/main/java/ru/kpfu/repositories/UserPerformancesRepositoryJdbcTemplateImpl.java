package ru.kpfu.repositories;
//
//import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
//import ru.kpfu.models.UserPerformances;
//
//import javax.sql.DataSource;
//import java.util.Optional;
//
//public class UserPerformancesRepositoryJdbcTemplateImpl extends UserPerformancesRepository {
//
//    private JdbcTemplate jdbcTemplate;
//    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
//
//    //language = SQL
//    private final static String SQL_SELECT = "SELECT * FROM performances WHERE id = ?";
//
//    private RowMapper<UserPerformances> userPerformancesRowMapper = (row, i) -> UserPerformances.builder()
//            .id(row.getInt("id"))
//            .animals(row.getBoolean("animals"))
//            .luggage(row.getBoolean("luggage"))
//            .music(row.getBoolean("music"))
//            .smoke(row.getBoolean("smoke"))
//            .talks(row.getBoolean("talks"))
//            .build();
//
//    public UserPerformancesRepositoryJdbcTemplateImpl(DataSource dataSource) {
//        this.jdbcTemplate = new JdbcTemplate(dataSource);
//        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
//    }
//
//    public Optional<UserPerformances> findById(Long id) {
//        UserPerformances user;
//        try {
//            user = jdbcTemplate.queryForObject(SQL_SELECT, userPerformancesRowMapper, id);
//        }
//        catch (EmptyResultDataAccessException e) {
//            user = null;
//        }
//
//        return Optional.ofNullable(user);
//    }
//}
