package ru.kpfu.models;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "profile")
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    private String birthday;

    private String number;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(referencedColumnName = "id")
    @Transient
    private User user;

    @OneToOne()
    @JoinColumn(referencedColumnName = "id")
    private UserPerformances performances;

    public Profile(Long id) {
        this.id = id;
    }

    public Profile(String name, String birthdate, String number, User user, UserPerformances performances) {
        this.name = name;
        this.birthday = birthdate;
        this.number = number;
        this.user = user;
        this.performances = performances;
    }
}
