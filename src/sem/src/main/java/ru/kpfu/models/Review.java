package ru.kpfu.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@Builder
public class Review {

    private Long id;
    private Long profileId;
    private Byte mark;
    private String text;
    private String authorName;
}
