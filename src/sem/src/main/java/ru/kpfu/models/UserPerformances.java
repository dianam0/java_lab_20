package ru.kpfu.models;


import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "performances")
public class UserPerformances {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id = 1;
    private Boolean animals;
    private Boolean talks;
    private Boolean music;
    private Boolean smoke;
    private Boolean luggage;

    public UserPerformances(String animals, String talks, String music, String luggage, String smoke) {

        this.animals = animals.equals("true");
        this.talks = talks.equals("true");
        this.music = music.equals("true");
        this.luggage = luggage.equals("true");
        this.smoke = smoke.equals("true");

        if (this.luggage) {
            id++;
        }
        if (this.smoke) {
            id += 2;
        }
        if (this.music) {
            id += 4;
        }
        if (this.talks) {
            id += 8;
        }
        if (this.animals) {
            id += 16;
        }
    }
}
