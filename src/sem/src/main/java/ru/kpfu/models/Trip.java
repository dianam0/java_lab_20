package ru.kpfu.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.Getter;
import lombok.ToString;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
public class Trip {

    private Long id;
    private Profile driver;
    private Profile companion1;
    private Profile companion2;
    private String pointFrom;
    private String pointTo;
    private String date;
    private String time;
    private String transport;
    private Integer price;

    public Trip(Profile driver, String pointFrom, String pointTo, String date, String time, Integer price) {
        this.driver = driver;
        this.pointFrom = pointFrom;
        this.pointTo = pointTo;
        this.date = date;
        this.time = time;
        this.price = price;
    }

}



