package ru.kpfu.models;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Builder;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "method_execution")
public class MethodExecution {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    public String signature;
    @Column(name = "class_name")
    public String className;
    @CreationTimestamp
    @Column(name = "inviked_at")
    private LocalDate invokationDate;
}
