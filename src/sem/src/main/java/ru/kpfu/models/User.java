package ru.kpfu.models;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.*;

@EqualsAndHashCode
@Getter
@Setter
@ToString
@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDate creationDate;

    @Column(name = "hash_password")
    private String hashPassword;
    private String email;

    @Column(name = "confirm_code")
    private String confirmCode;

    @Enumerated(value = EnumType.STRING)
    private State state;
    @Enumerated(value = EnumType.STRING)
    private Role role;


    public enum State {
        NOT_CONFIRMED,
        CONFIRMED,
        BANNED
    }

    public enum Role {
        USER,
        ADMIN
    }

    public boolean isAdmin() {
        return this.role == Role.ADMIN;
    }

    public boolean isActive() {
        return this.state != State.BANNED;
    }
}
