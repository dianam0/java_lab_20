package ru.kpfu.aop.services;

public interface MethodExecutionService {
    void save(String signature, String className);
}
