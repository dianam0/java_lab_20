package ru.kpfu.aop.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.aop.services.MethodExecutionService;

@Aspect
public class MethodExecutionAspect {

    @Autowired
    private MethodExecutionService service;

    @Pointcut("within(ru.kpfu.services..*)")
    public void method() {}

    @Around("method()")
    public void beforeMethod(ProceedingJoinPoint joinPoint) throws Throwable {
       String signature = joinPoint.getSignature().toString();
       String className = joinPoint.getTarget().getClass().toString();

       service.save(signature, className);
       joinPoint.proceed();
    }
}
