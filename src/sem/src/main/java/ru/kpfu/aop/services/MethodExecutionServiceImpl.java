package ru.kpfu.aop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.aop.services.MethodExecutionService;
import ru.kpfu.models.MethodExecution;
import ru.kpfu.repositories.MethodExecutionRepository;

@Service
public class MethodExecutionServiceImpl implements MethodExecutionService {

    @Autowired
    private MethodExecutionRepository repository;

    @Override
    public void save(String signature, String className) {
        repository.save(MethodExecution.builder()
                .signature(signature)
                .className(className)
                .build()
        );
    }
}
