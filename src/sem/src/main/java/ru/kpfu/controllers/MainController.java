package ru.kpfu.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.security.PermitAll;

@Controller()
public class MainController {

    @PermitAll
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "text/html; charset='utf-8'")
    public String main() {
        return "main";
    }
}

