package ru.kpfu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.dto.TripDto;
import ru.kpfu.models.Trip;
import ru.kpfu.services.ProfileService;
import ru.kpfu.services.TripService;

import java.util.List;

@Controller
public class TripController {

    @Autowired
    private TripService tripService;

    @Autowired
    private ProfileService profileService;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createTripPage() {
        return "proposal";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchTripPage() {
        return "search";
    }

    @RequestMapping(value = "/trips", method = RequestMethod.GET)
    public String searchTrip(@RequestParam(value = "pointFrom") String from,
                             @RequestParam(value = "pointTo") String to,
                             @RequestParam(value = "date") String date,
                             Model model) {
        List<Trip> trips = tripService.getTripsByParams(from, to, date, 0, 1000000);
        if (trips != null &&!trips.isEmpty()) {
            model.addAttribute("trips", trips);
        }
        model.addAttribute("from", from);
        model.addAttribute("to", to);
        model.addAttribute("date", date);
        return "trips";
    }


    @RequestMapping(value = "/mytrips", method = RequestMethod.GET)
    public String myTripsPage() {
        return "redirect:/login";
    }

    @RequestMapping(value = "/trip", method = RequestMethod.GET)
    public String tripInfoPage(@RequestParam("id")String id,
                               @RequestParam("driver")String driver,
                               @RequestParam("companion1")String companion1,
                               @RequestParam("companion2")String companion2,
                               @RequestParam("pointFrom")String pointFrom,
                               @RequestParam("pointTo")String pointTo,
                               @RequestParam("date")String date,
                               @RequestParam("time")String time,
                               @RequestParam("price")String price,
                               @RequestParam("transport")String transport,
                               Model model) {
        Trip trip = Trip.builder()
                .date(date)
                .pointFrom(pointFrom)
                .pointTo(pointTo)
                .transport(transport)
                .time(time)
                .price(5000)
                .driver(profileService.getProfileById(Long.parseLong(driver)).get())
                .build();

        if (companion1 != null && !companion1.equals("0")) {
            trip.setCompanion1(profileService.getProfileById(Long.parseLong(companion1)).orElse(null));
            trip.setCompanion2(profileService.getProfileById(Long.parseLong(companion2)).orElse(null));
        }

        model.addAttribute("trip", trip);
        return "trip";
    }

    @RequestMapping(value = "/filter", method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Trip> filter(@RequestBody TripDto trip) {
        List<Trip> trips = tripService.getTripsByParams(trip.getPointFrom(), trip.getPointTo(), trip.getDate(), Integer.parseInt(trip.getPriceFrom()), Integer.parseInt(trip.getPriceTo()));
        System.out.println(trips);
        if (!trips.isEmpty()) {
            return trips;
        }

        return null;
    }
}
