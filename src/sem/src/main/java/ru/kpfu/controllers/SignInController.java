package ru.kpfu.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.security.PermitAll;

@Controller
public class SignInController {

    @PermitAll
    @RequestMapping(method = {RequestMethod.GET}, value = "/logIn")
    public String getSignInPage() {
        return "login";
    }
}
