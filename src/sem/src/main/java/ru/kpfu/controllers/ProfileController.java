package ru.kpfu.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.kpfu.security.details.UserDetailsImpl;
import ru.kpfu.services.ProfileService;

import javax.annotation.security.PermitAll;

@Controller
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @PreAuthorize("isAuthenticated()")
    @PermitAll
    @GetMapping("/profile")
    public String getProfilePage(@AuthenticationPrincipal UserDetailsImpl user, Model model) {
        model.addAttribute("profile", user);
        return "profile";
    }
}
