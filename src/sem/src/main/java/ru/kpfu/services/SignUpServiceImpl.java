package ru.kpfu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.kpfu.dto.UserForm;
import ru.kpfu.models.User;
import ru.kpfu.models.UserPerformances;
import ru.kpfu.repositories.ProfileRepository;
import ru.kpfu.repositories.UserRepository;
import ru.kpfu.utils.email.EmailUtil;
import ru.kpfu.utils.email.MailsGenerator;

import javax.transaction.Transactional;
import java.util.UUID;

@Profile("master")
@Service
@Transactional
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailUtil emailUtil;

    @Autowired
    private MailsGenerator mailsGenerator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${server.url}")
    private String serverUrl;

    @Value("${spring.mail.username}")
    private String from;


    public SignUpServiceImpl(UserRepository userRepository, ProfileRepository profileRepository) {
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
    }

    @Override
    public Boolean signUp(UserForm userForm) {
        User newUser = User.builder()
                .email(userForm.getEmail())
                .hashPassword(passwordEncoder.encode(userForm.getPassword()))
                .state(User.State.NOT_CONFIRMED)
                .confirmCode(UUID.randomUUID().toString())
                .role(User.Role.USER)
                .build();
        newUser = userRepository.save(newUser);

        if (newUser.getId() == null) {
            return false;
        }

        UserPerformances userPerformances = new UserPerformances(userForm.getAnimals(), userForm.getTalks(), userForm.getMusic(), userForm.getLuggage(), userForm.getSmoke());

        ru.kpfu.models.Profile profile = ru.kpfu.models.Profile.builder()
                .id(newUser.getId())
                .name(userForm.getName())
                .number(userForm.getNumber())
                .birthday(userForm.getBirthday())
                .performances(userPerformances)
                .user(newUser)
                .build();

        profileRepository.save(profile);

        String confirmMail = mailsGenerator.getMailForConfirm(serverUrl, newUser.getConfirmCode());

        emailUtil.sendMail(newUser.getEmail(), "Регистрация", from, confirmMail);
        return true;
    }
}
