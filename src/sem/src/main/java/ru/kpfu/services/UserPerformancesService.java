package ru.kpfu.services;

import ru.kpfu.models.UserPerformances;

import java.util.Optional;

public interface UserPerformancesService {

    Optional<UserPerformances> getUserPerformancesById(Integer id);
}
