package ru.kpfu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.models.Trip;
import ru.kpfu.repositories.TripRepository;

import java.util.List;

@Service
public class TripServiceImpl implements TripService {

    @Autowired
    private TripRepository tripRepository;

    public TripServiceImpl(TripRepository tripRepository) {
        this.tripRepository = tripRepository;
    }

    @Override
    public List<Trip> getTripsByParams(String from, String to, String date, Integer priceFrom, Integer priceTo) {
        return tripRepository.findByRouteDateAndPrice(from, to, date, priceFrom, priceTo);
    }
}
