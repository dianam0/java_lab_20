package ru.kpfu.services;

import ru.kpfu.dto.UserForm;

public interface SignUpService {

    Boolean signUp(UserForm userForm);

}
