package ru.kpfu.services;

import ru.kpfu.models.Review;

import java.util.List;

public interface ReviewService {

    List<Review> getAllByUserId(Long id);
}
