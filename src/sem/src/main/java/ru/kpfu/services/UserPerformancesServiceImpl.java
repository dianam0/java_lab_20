//package ru.kpfu.services;
//
//import org.springframework.stereotype.Service;
//import ru.kpfu.models.UserPerformances;
//import ru.kpfu.repositories.UserPerformancesRepository;
//
//import java.util.Optional;
//
//@Service
//public class UserPerformancesServiceImpl implements UserPerformancesService {
//
//    private UserPerformancesRepository userPerformancesRepository;
//
//    public UserPerformancesServiceImpl(UserPerformancesRepository userPerformancesRepository) {
//        this.userPerformancesRepository = userPerformancesRepository;
//    }
//
//    @Override
//    public Optional<UserPerformances> getUserPerformancesById(Integer id) {
//        return this.userPerformancesRepository.findById(id.longValue());
//    }
//}
