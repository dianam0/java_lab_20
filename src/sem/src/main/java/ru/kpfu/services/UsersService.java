package ru.kpfu.services;

import ru.kpfu.models.User;

import java.util.List;

public interface UsersService {

    List<User> getAllUsers();

    void banAll();
}
