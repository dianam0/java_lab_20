package ru.kpfu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.dto.UserForm;
import ru.kpfu.models.Profile;
import ru.kpfu.models.User;
import ru.kpfu.models.UserPerformances;
import ru.kpfu.repositories.ProfileRepository;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Optional;

@Service
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private ProfileRepository profileRepository;

    public  ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    public Optional<Profile> getProfileById(Long id) {
        return profileRepository.findById(id);
    }
}
