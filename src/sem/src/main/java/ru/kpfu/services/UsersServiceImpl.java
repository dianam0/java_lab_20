package ru.kpfu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.models.User;
import ru.kpfu.repositories.UserRepository;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UserRepository repository;

    @Override
    public List<User> getAllUsers() {
        return repository.findUsersByStateIsNotNull();
    }

    @Override
    public void banAll() {
        List<User> users = repository.findAll();

        for (User user : users) {
            if (!user.isAdmin()) {
                user.setState(User.State.BANNED);
                repository.save(user);
            }
        }
    }

}
