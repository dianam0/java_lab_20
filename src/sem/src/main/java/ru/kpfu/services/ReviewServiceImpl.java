package ru.kpfu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.models.Review;
import ru.kpfu.repositories.ReviewRepository;

import java.util.List;

@Service
public class ReviewServiceImpl implements  ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<Review> getAllByUserId(Long id) {
        return reviewRepository.findAllByUserId(id);
    }
}
