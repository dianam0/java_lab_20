package ru.kpfu.services;

import ru.kpfu.dto.UserForm;
import ru.kpfu.models.Profile;

import java.util.Optional;

public interface ProfileService {

    Optional<Profile> getProfileById(Long id);

}
