package ru.kpfu.services;

import ru.kpfu.models.Trip;

import java.util.List;

public interface TripService {

    List<Trip> getTripsByParams(String from, String to, String date, Integer priceFrom, Integer priceTo);
}
