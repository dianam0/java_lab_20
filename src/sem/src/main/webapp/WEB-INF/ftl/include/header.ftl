<nav class="navbar navbar-expand-sm navbar-light" style="background-color: #c1d4ee;">
    <a class="navbar-brand col-md-7" href="#">
        <img src="images/auto-icon.png" width="40" height="40" class="d-inline-block align-top" alt=""
             loading="lazy">
        beep-beep.ru
    </a>
    <ul class="navbar-nav arrows-on-right-horizontal">
        <li class="nav-item">
            <a class="nav-link" href="main">Main</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="search">Find a ride</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="create">
                Create a request</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="mytrips">My trips</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="profile">My profile</a>
        </li>
    </ul>
</nav>