function sendAjax() {
    let data = {
        "pointFrom" : check("pointFrom"),
        "pointTo": check("pointTo"),
        "date" : check("date"),
        "priceFrom" : check("from"),
        "priceTo" : check("to")
    };
    hideTrips();
    $.ajax({
        type: "POST",
        url: "/sem_war_exploded/filter",
        data: JSON.stringify(data),

        success: function (response) {
            update(response);
        },

        dataType: "json",
        contentType: "application/json"
    });
}

function check(data) {
    return document.getElementById(data).value;
}

function hideTrips() {
    let trip = document.getElementsByClassName("trip");
    for (let i = 0; i < trip.length; i++) {
        trip[i].setAttribute("hidden", "hidden");
    }
}

function update(trips) {
    for (let i = 0; i < trips.length; i++) {
        document.getElementById(trips[i]['id']).hidden = false;
    }
}